﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using DataBindingDemo.BusinessLogic;
namespace DataBindingDemo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CaptureDataFromUIWithDataBinding : ContentPage
    {
        User user;
        public CaptureDataFromUIWithDataBinding()
        {
            InitializeComponent();
            user = new User("test@gmail.com", "tested");
            this.BindingContext = user;
        }

        private void OnDisplay(object sender, EventArgs e)
        {
            DisplayAlert("User info", $"{user.Email}, {user.Password}", "Ok");
        }
    }
}