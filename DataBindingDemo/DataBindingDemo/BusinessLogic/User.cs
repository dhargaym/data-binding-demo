﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBindingDemo.BusinessLogic
{
    public class User
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public User(string email, string password)
        {
            Email = email;
            Password = password;
        }

        public override string ToString()
        {
            return $"{Email}, {Password}";
        }
    }
}
