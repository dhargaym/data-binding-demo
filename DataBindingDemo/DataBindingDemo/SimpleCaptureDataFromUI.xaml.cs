﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DataBindingDemo.BusinessLogic;

namespace DataBindingDemo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimpleCaptureDataFromUI : ContentPage
    {
        User user;
        public SimpleCaptureDataFromUI()
        {
            InitializeComponent();
            user = new User("test@gmail.com", "tested");
            TxtEmail.Text = user.Email;
            TxtPassword.Text = user.Password;
        }

        private void OnEmailChanged(object sender, TextChangedEventArgs e)
        {
            user.Email = TxtEmail.Text;
        }

        private void OnPasswordChanged(object sender, TextChangedEventArgs e)
        {
            user.Password = TxtPassword.Text;
        }
        private void OnDisplay(object sender, EventArgs e)
        {
            DisplayAlert("User info", $"{user.Email}, {user.Password}", "Ok");
        }

    }
}