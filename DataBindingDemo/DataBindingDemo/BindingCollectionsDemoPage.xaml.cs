﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using DataBindingDemo.BusinessLogic;

namespace DataBindingDemo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BindingCollectionsDemoPage : ContentPage
    {
        List<User> users = new List<User>();
        public BindingCollectionsDemoPage()
        {
            InitializeComponent();
            //TODO:
            //Render the users using a custom layout,
            //showing the profile picture of each user in the list view
            //Assume the user list is fixed (since we do not know hoe to interact with the file system)
            //Assume the profile picture name is <username>.png
            //Extract the username from the email
            users.Add(new User("user1@gmail.com", "pass1"));
            users.Add(new User("user2@gmail.com", "pass2"));
            users.Add(new User("user3@gmail.com", "pass3"));
            users.Add(new User("user4@gmail.com", "pass4"));
            PicUser.ItemsSource = users;
            LstUser.ItemsSource = users;    
        }

        private void OnDisplayCurrent(object sender, EventArgs e)
        {
            if (PicUser.SelectedItem != null && LstUser.SelectedItem!=null  )
            {
                string message = "";
                message += ((User)PicUser.SelectedItem).ToString();
                message += "\n";
                message += ((User)LstUser.SelectedItem).ToString();
                DisplayAlert("Selection", message, "Ok");
            }
            else
            {
                DisplayAlert("Selection", "Select a user from the picker and the list first", "Ok");
            }

        }
    }
}