﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DataBindingDemo
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimpleRegistrationPage : ContentPage
    {
        public SimpleRegistrationPage()
        {
            InitializeComponent();
        }

        private void OnAgreeChanged(object sender, CheckedChangedEventArgs e)
        {
            //if (ChkAgree.IsChecked)
            //    BRegister.IsEnabled = true;
            //else
            //    BRegister.IsEnabled = false;
            BRegister.IsEnabled = ChkAgree.IsChecked;
        }
    }
}